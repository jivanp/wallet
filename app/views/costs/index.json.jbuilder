json.array!(@costs) do |cost|
  json.extract! cost, :id, :expense, :category, :amount, :spend_date
  json.url cost_url(cost, format: :json)
end
json.array!(@costs_total) do |cost|
  json.extract! cost, :category, :amount
  json.url cost_url(cost, format: :json)
end
