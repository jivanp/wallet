json.extract! @cost, :id, :expense, :category, :amount, :spend_date, :created_at, :updated_at
json.extract! @costs_total, :expense, :amount
