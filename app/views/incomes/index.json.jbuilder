json.array!(@incomes) do |income|
  json.extract! income, :id, :expense, :category, :amount, :earn_date
  json.url income_url(income, format: :json)
end
