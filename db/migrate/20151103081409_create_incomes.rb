class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.string :expense
      t.string :category
      t.integer :amount
      t.date :earn_date

      t.timestamps
    end
  end
end
