class CreateCosts < ActiveRecord::Migration
  def change
    create_table :costs do |t|
      t.string :expense
      t.string :category
      t.integer :amount
      t.date :spend_date

      t.timestamps
    end
  end
end
